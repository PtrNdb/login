import hashlib

import SQL

def login():
    email = input("Podaj adres email")
    password = input("Podaj hasło")
    hashed_pass = hashlib.sha256(password.encode("utf-8")).hexdigest()

    user = SQL.find_user_by_email_and_password(email, hashed_pass)

    if len(user) == 0:
        print("Nie ma takiego uzytkownika w bazie danych z podanym emailem lub haslem")
    else:
        print(user)

    menu()


def register():
    email  = input("Podaj adres email")
    name = input("Podaj imie")
    password1 = input("Podaj haslo")
    password2 = input("Powtorz haslo")
    salary = input("Podaj Zarobki")
    print("podano wszystkie informacje")

    if password1 != password2:
        print("Podano rozne hasla")
        menu()

    hashed_pass = hashlib.sha256(password1.encode("utf-8")).hexdigest()
    SQL.create_user(email, name, hashed_pass, salary)
    print("teraz przejde do menu")
    menu()


def show_users():
    print(SQL.get_all_users())
    menu()


def change_password():
    email = input("podaj adres email")
    old_password = input("podaj stare haslo")
    new_password = input("podaj nowe haslo")
    repeat_new_password = input("powtorz nowe haslo")

    old_hashed_password = hashlib.sha256(old_password.encode("utf-8")).hexdigest()
    user_list = SQL.find_user_by_email_and_password(email, old_hashed_password)
    if not len(user_list) > 0:
        print("nie ma takiego uzytkownika")
        menu()

    if new_password != repeat_new_password:
        print("wpisane nowe haslo jest niezgodne")
        menu()

    new_hashed_password = hashlib.sha256(new_password.encode("utf-8")).hexdigest()

    SQL.reset_user_password(email, new_hashed_password)
    print("Zmieniono haslo")
    menu()


def delete_user():
    email = input("Podaj adres email")
    password = input("Podaj haslo")

    hashed_password = hashlib.sha256(password.encode("utf-8")).hexdigest()

    user_list = SQL.find_user_by_email_and_password(email, hashed_password)

    if not len(user_list) > 0:
        print("Podano nieprawidlowy login lub haslo")
        menu()

    SQL.delete_user(email)
    print("usunieto konto")
    menu()

def salary():
    print(SQL.salary())
    menu()




def menu():
    print("""
1. Logowanie
2. Rejestracja
3. Lista użytkowników
4. Zmien haslo
5. Usun konto 
6. Zarobki
7. Menu
""")
    choise = int(input())

    if(choise == 1):
        login()
    if(choise == 2):
        register()
    if(choise == 3):
        show_users()
    if(choise == 4):
        change_password()
    if(choise == 5):
        delete_user()
    if (choise == 6):
        salary()
    if (choise == 7):
        menu()
menu()